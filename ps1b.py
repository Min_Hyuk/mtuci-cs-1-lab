Annual_salary = float(input('Enter your salary: '))
portion_saved = float(input('Enter the part of the salary to keep: '))
total_cost = float(input('Enter the cost of the house: '))
semi_annual_raise = float(input('Enter the salary increase: '))

partition_down_payment = 0.25 * total_cost
current_savings = 0
r = 0.04
month = 0

while current_savings < partition_down_payment:
    current_savings += (Annual_salary / 12 * portion_saved) + (current_savings / 12 * r)
    month += 1
    if (month > 1) and (month % 6 == 0):
        Annual_salary += Annual_salary * semi_annual_raise

print(month)
